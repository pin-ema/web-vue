import BrowsePage from './BrowsePage.vue';
import NewPage from './NewPage.vue';

export {
  BrowsePage,
  NewPage,
};
